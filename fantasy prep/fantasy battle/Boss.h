//
// Created by Andrew Donson  on 12/4/16.
//

#ifndef FANTASY_BATTLE_BOSS_H
#define FANTASY_BATTLE_BOSS_H


class Boss {
public:
    int health;   // this is boss health
    int power;    // attack points of the boss
    int reward;   // amount of gold given by boss



    int bossattack();
    int defend();
    int givereward();
};


#endif //FANTASY_BATTLE_BOSS_H
