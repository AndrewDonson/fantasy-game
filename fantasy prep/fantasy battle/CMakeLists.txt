cmake_minimum_required(VERSION 3.6)
project(fantasy_battle)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Store.cpp Store.h Weapons.cpp Weapons.h armor.cpp armor.h health.cpp health.h .idea/store.cpp salabe.cpp salabe.h stock.cpp stock.h shoppingcart.cpp shoppingcart.h Player.cpp Player.h .idea/Inventory.cpp .idea/Inventory.h)
add_executable(fantasy_battle ${SOURCE_FILES})