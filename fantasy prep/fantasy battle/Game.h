//
// Created by Andrew Donson  on 12/4/16.
//

#ifndef FANTASY_BATTLE_GAME_H
#define FANTASY_BATTLE_GAME_H


class Game {
public:

    int save;  //this will be where the game will be saved

    void instructions();
    void loadgame();
    void savegame();
    void gotoarena();
    void gotostore();

};


#endif //FANTASY_BATTLE_GAME_H
