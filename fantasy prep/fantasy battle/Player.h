//
// Created by Andrew Donson  on 12/4/16.
//

#ifndef FANTASY_BATTLE_PLAYER_H
#define FANTASY_BATTLE_PLAYER_H


class Player {
public:

    int attackpower;  // the damage the player deals
    int armor;        // this increases health
    int health;       // players health

    int attack();
    void inv();


};


#endif //FANTASY_BATTLE_PLAYER_H
