//
// Created by Andrew Donson  on 11/9/16.
//

#include "salabe.h"

salabe::salabe(string type,string name, int power, int cost);

// gives type
string salabe::showType()
{
    return type;

}
// gives name
string salabe::showName()
{
    return name;

}
//gives power
int salabe::showPower()
{
    return power;

}
//gives cost
int salabe::showCost()
{
    return cost;

}

ostream& operator<<(ostream& output, shared_ptr<salabe>& newwapeaon);