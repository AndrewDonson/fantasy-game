//
// Created by Andrew Donson  on 11/9/16.
//

#ifndef FANTASY_BATTLE_SALABE_H
#define FANTASY_BATTLE_SALABE_H
#include <string>
#include <ostream>
using namespace std;
class salabe {
public:

    //all varabiles for the item brought in
    string type;
    string name;
    int power;
    int cost;
    // gives the iteams all varibles
    salabe();
    salabe(string type, string name, int power,int cost);



    //this will print all functions
    string showType();
    string showName();
    int showPower();
    int showCost();

    // shares across the classes
    friend ostream& operator<<(ostream& output, shared_ptr<salabe>& store);
};


#endif //FANTASY_BATTLE_SALABE_H
