//
// Created by Andrew Donson  on 12/13/16.
//

#ifndef FANTASY_BATTLE_ARMOR_H
#define FANTASY_BATTLE_ARMOR_H
#include "iteams.h"

class armor: public iteams {
    armor(string name, string type, int power, int cost, int value);


    //shares the charateritcs of armor
    friend ostream& operator<<(ostream& output, shared_ptr<armor>& item);
};

};


#endif //FANTASY_BATTLE_ARMOR_H
