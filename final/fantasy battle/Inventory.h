//
// Created by Andrew Donson  on 12/11/16.
//

#ifndef FANTASY_BATTLE_INVENTORY_H
#define FANTASY_BATTLE_INVENTORY_H
#include<vector>
#include<iostream>
using namespace std;
class Inventory {
public:

    string armor = "copper armor";
    string weapon = "basic hammer";
    int coins = 0;


    void playerInventory(string armor, string weapon,int coins);
    void showinv();
    void addhealth();
    void addattack();


};


#endif //FANTASY_BATTLE_INVENTORY_HSY_BATTLE_INVENTORY_H
