//
// Created by Andrew Donson  on 12/11/16.
//

#include "Monster.h"
#include "Inventory.h"
#include "Player.h"




/*
 *
 *  this will let the boss attack the user on an int system
 */
int Monster::bossattack()
{
    cout << "boss attacks player for " << power << endl;
    return power;

}


/*
 * this will random switch with attack
 * if used it will defend the boss for no damage
 */
int Monster::defend()
{


    cout << "the boss blocks the players attack and takes little damage" << endl;
    health += 10;
    return 0;


}



int Monster::getsHurt(int x)
{
    cout << name << "takes" << x << "damage from player" << endl;
    health -= x;
    return health;
}




/*
 * this will give the player goal when the boss is defeted
 *
 */
int Monster::giveReward()
{

    return reward;

}


int Monster::showhealth()
{

    return health;
}




void Monster::showBossStats()
{
    cout << name << endl;
    cout << "health: " << health << endl;
    cout << "Power: " << power << endl;
    cout << "reward: " << reward << endl;


}


steve::steve()
{
    name = "steve";
    reward = 50;
    health = 100;
    power = 50;

}


mark::mark()
{
    name = "mark";
    reward = 150;
    health = 250;
    power = 100;

}

micheal::micheal()
{
    name = "micheal";
    reward = 200;
    health = 275;
    power = 125;

}

UltimateFighter::UltimateFighter()
{
    name = "UltimateFighter";
    reward = 10000;
    health = 500;
    power = 75;
}
