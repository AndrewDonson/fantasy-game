//
// Created by Andrew Donson  on 12/11/16.
//

#ifndef FANTASY_BATTLE_MONSTER_H
#define FANTASY_BATTLE_MONSTER_H
#include<iostream>
#include<string>
using namespace std;

class Monster {
protected:
    int health = 50;   // this is boss health
    int power = 5;    // attack points of the boss
    string name;
    int reward = 10;   // amount of gold given by boss
    int BossChoice = 0;

public:





    void showBossStats();
    int bossattack();
    int defend();
    int getsHurt(int x);
    int showhealth();
    void display();
    int giveReward();
};

class steve: public Monster
{
public:
    steve();
};



class joe: public Monster
{
public:
    joe();
};



class micheal: public Monster
{
public:
    micheal();
};


class mark: public Monster
{
public:
    mark();
};


class UltimateFighter: public Monster
{
public:
    UltimateFighter();
};

#endif //FANTASY_BATTLE_MONSTER_H
