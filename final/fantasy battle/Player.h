//
// Created by Andrew Donson  on 12/4/16.
//

#ifndef FANTASY_BATTLE_PLAYER_H
#define FANTASY_BATTLE_PLAYER_H


#include "Inventory.h"

class Player: public Inventory {
public:

    int attackpower = 15;  // the damage the player deals
    int armor = 0;        // this increases health
    int Playerhealth = 75;      // players health
    int restore = Playerhealth;


    int attack();
    int getHit(int x);
    void reset();
    int getPlayerhealth();
    void showinv();
    void saveFile(Inventory playerInventory, Player *player);
    virtual void showStats();

};



#endif //FANTASY_BATTLE_PLAYER_H
