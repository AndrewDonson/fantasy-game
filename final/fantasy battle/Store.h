//
// Created by Andrew Donson  on 11/4/16.
//

#ifndef FANTASY_BATTLE_STORE_H
#define FANTASY_BATTLE_STORE_H
#include "Inventory.h"
#include "object.h"
#include <memory>

class Store: public Inventory {
public:

    /*
     * Sort the items by their names
     */
    void sort();

    /*
     * Print operator overload prints out all the items in store into a list
     */
    friend ostream& operator<<(ostream& output, Store& store);
};

#endif //FANTASY_BATTLE_STORE_H
