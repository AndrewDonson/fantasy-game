//
// Created by Andrew Donson  on 12/12/16.
//

#include "iteam.h"
iteam::iteam(string name, string type, int power, int cost):
        name(name), type(type), power(power), cost(cost){};
//shows the cost
int iteam::showCost()  {
    return cost;
}
// gives the name of object
string iteam::showName()  {
    return name;
}
// type of iteam like weapon
string iteam::showType()  {
    return type;
}
// the attack power of the item
int iteam::showPower()  {
    return power;
}


// shares the iteams across classes
ostream& operator<<(ostream& output, shared_ptr<object>& item){
    output << item->showStats() << endl;
    return output;
}
