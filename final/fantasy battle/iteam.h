//
// Created by Andrew Donson  on 12/12/16.
//

#ifndef FANTASY_BATTLE_ITEAM_H
#define FANTASY_BATTLE_ITEAM_H
#include <ostream>
#include <string>
#include <memory>
using namespace std;

class object {
public:

    // base stats for the items
    int power;
    int cost;
    string name;
    string type;


    iteam();

    // creating and intanst of the iteam
    iteam(string name, string type, int power, int cost);

    int showCost();
    string showName();
    string showType();
    int showPower();




     //prints and shares the iteams
    friend ostream& operator<<(ostream& output, shared_ptr<iteam>& item);

};


#endif //FANTASY_BATTLE_ITEAM_H
