#include <iostream>
#include"Game.h"
#include "Player.h"
#include "Monster.h"
#include "arena.h"
#include "weapon.h"
#include "Store.h"
#include "Inventory.h"
#include "armor.h"
#include <fstream>
#include <sstream>


using namespace std;


/*
 *
 * this will load the prevoius save game
 *
 */
void loadgame()
{
    vector<string> load;
    ifstream infile("Saveplayer.txt");
    while(infile)
    {
        string save;
        if (!getline(infile,save)) break;
        istringstream saved(save);
        while(saved)
        {
            string save;
            if (!getline(saved,save,'|'))break;
            load.push_back(save);
        }
    }
    if (!infile.eof())
    {
        cout << "COULD NOT FIND FILE" << endl;

    }
}

/*
 * this will save users game to a file
 *
 */
void savegame()
{
    fstream file;
    file.open("Saveplayer.txt");

    file << "player inventory: " << Player->playerInventory();
    file.close();

}





void getStore(string file) {

    string fileAddress;

    ifstream infile;
    infile.open(fileAddress, ios::in);

    string line;
    cout << "Reading the data.." << endl;
    while (getline(infile, line)) {
        string name, type, cost, power;
        istringstream lineStream(line);
        getline(lineStream, name, '|');
        getline(lineStream, type, '|');
        getline(lineStream, cost, '|');
        getline(lineStream, power, '|');


        if (type == "weapon") {
            auto item = make_shared<weapon>(name, type, power, cost);
            Store.add(item);
        } else if (type == "armor") {
            auto item = make_shared<armor>(name, type, power, cost);
            Store.add(item);
        }


        infile.close();
    }
}



int main() {
    cout << "welcome to the battle arena!" << endl;
    cout << "here you will fight monsters and use the store to buy upgrades" << endl;
    cout << "Dont forget to save your progress!! After every buy or battle" << endl;
    cout << "" << endl;

    cout << "Where would you like to go" << endl;
    cout << "1: arena, 2: store 3: save" << endl;

    string game = "true";
    while (game == "true") {
        string choice;
        cin >> choice;


        if (choice == "1") {

            // makes all of the bosses 
            joe Joe;
            steve Steve;
            micheal Micheal;
            mark Mark;
            UltimateFighter ultimatefighter;

            //sets up arena and the battle for the player
            arena();
            arena::selectBattle();
            
        //goes to store and makes the store 
        else if(choice == "2")
            {
                getStore("newweapon.txt");
                Store();
            }
                
            
        
        // saves your game 
        else if (choice == 3) {
            savegame();
        }
            
        else if (choice == "4")
            {
                
                loadgame();
            }
        else if ("")
            {
                cout >> "sorry incorrect input" >> endl;
                
            }
            
            return 0;
    }
}


};