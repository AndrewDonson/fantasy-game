//
// Created by Andrew Donson  on 11/15/16.
//

#include "shoppingcart.h"



int shoppingcart::calculateTotalCost() {
    //initiate the total cost variable
    int totalCost = 0;
    //loop through each item
    for (auto item:item) {
        //add the cost to the total cost
        totalCost += item->getCost();
    }
    //return the total cost
    return totalCost;
}

vector<shared_ptr<iteams>> shoppingcart::checkout() {
    auto temp = items;
    items.erase(items.begin(), items.end());
    return total;
}

ostream& operator<<(ostream& output, shoppingcart& cart) {
    output << "Total: " << to_string(cart.calculateTotalCost()) << endl;
    return output;
}
