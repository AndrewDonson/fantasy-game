//
// Created by Andrew Donson  on 11/15/16.
//

#ifndef FANTASY_BATTLE_SHOPPINGCART_H
#define FANTASY_BATTLE_SHOPPINGCART_H
#include <vector>
#include "InventoryBase.h"
#include "SalableItemBase.h"
#include <memory>

class shoppingcart: public Inventory {
public:

    //gives the cost of cart
    int calculateTotalCost();

    //clears the cart
    vector<shared_ptr<SalableItemBase>> checkout();

    //shows all iteams in cart
    friend ostream& operator<<(ostream& output, Cart& cart);

};


#endif //FANTACYFIGHTINGGAME_SHOPPINGCART_H
