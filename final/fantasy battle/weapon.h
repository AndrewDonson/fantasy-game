//
// Created by Andrew Donson  on 12/11/16.
//

#ifndef FANTASY_BATTLE_WEAPON_H
#define FANTASY_BATTLE_WEAPON_H
#include "object.h"
#include "iteam.h"

class weapon: public object {
public:
    weapon(string name, string type, int power, int cost, int value);


    //shares the charateritcs of armor

    friend ostream& operator<<(ostream& output, shared_ptr<weapon>& item);
};


#endif //FANTASY_BATTLE_WEAPON_H
